import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeFormPageComponent } from './income-form-page.component';

describe('IncomeFormPageComponent', () => {
  let component: IncomeFormPageComponent;
  let fixture: ComponentFixture<IncomeFormPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeFormPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
