import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ReportpageComponent } from './reportpage/reportpage.component';
import { ExpensesFormPageComponent } from './expenses-form-page/expenses-form-page.component';
import { IncomeFormPageComponent } from './income-form-page/income-form-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    ReportpageComponent,
    ExpensesFormPageComponent,
    IncomeFormPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
